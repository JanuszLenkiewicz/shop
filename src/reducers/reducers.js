import { combineReducers } from "redux";

import user from '../features/login/reducers/login.reducer';
import users from '../features/users/reducers/users.reducer';
import courses from '../features/create-course/reducers/create-course.reducer';

const reducers = combineReducers({
  user,
  users,
  courses
});

export default reducers;
