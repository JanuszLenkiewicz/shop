import React from 'react';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import { string, shape } from 'prop-types';

import classes from './formField.css'

const FormField = ({formData, change, id}) => {

  const showError = () => {
    let errorMessage = null;

    if(formData.validation && !formData.valid){
      errorMessage = (
        <FormHelperText id={formData.config.aria}>
          {formData.validationMessage}
        </FormHelperText>
      )
    }

    return errorMessage;
  };


  const renderTemplate = () => {
    let formTemplate = null;

    switch(formData.element){
      case('input'):
        formTemplate = (
          <div className="formBlock">
            <TextField
              id={formData.config.id}
              label={formData.config.name}
              type={formData.config.type}
              placeholder={formData.config.placeholder}
              className={classes.textField}
              value={formData.value}
              onBlur={(event)=> change({event,id,blur:true})}
              onChange={(event)=> change({event,id})}
              margin={formData.config.margin}
              aria-describedby={formData.config.aria}
            />
            <div className={classes.colorError}>
              {showError()}
            </div>


          </div>
        );
        break;
      default:
        formTemplate = null;
    }

    return formTemplate;
  };


  return (
    <div>
      {renderTemplate()}
    </div>
  );
};

FormField.propTypes = {
  value: string,
  config: shape({
    id: string.isRequired,
    name: string.isRequired,
    type: string.isRequired,
    placeholder: string,
    value: string,
    margin: string,
  })
};

export default FormField;
