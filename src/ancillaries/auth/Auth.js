import React, { Component } from 'react';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import { getUser } from './selectors/auth.selector';

export default function(ComposedClass,reload,adminRoute = null, path='/users'){
  class AuthenticationCheck extends Component {

    state = {
      loading: true
    };

    componentDidMount(){
      const isAuth = localStorage.getItem('isAuth');
      const role = localStorage.getItem('role');

      if(reload){
        if(isAuth && adminRoute && (role === '1' || role === 1)){
          this.props.history.push(path)
        } else{
            this.props.history.push('/login')
        }
      }

      this.setState({loading:false})
    }

    render() {
      if(this.state.loading){
        return (
          <div className="main_loader">
            <CircularProgress style={{color:'#2196F3'}} thickness={7}/>
          </div>
        )
      }

      return (
        <ComposedClass {...this.props} user={this.props.user}/>
      );
    }
  }

  function mapStateToProps(state){
    return {
      user: getUser(state)
    }
  }

  return connect(mapStateToProps)(AuthenticationCheck)
}
