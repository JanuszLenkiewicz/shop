import React, {Fragment} from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';

import { Header } from '../../core/header';
import { Footer } from '../../core/footer';
import classes from './layout.css';

const Layout = (props) => {
  return (
    <Fragment>
      <Header/>
      <div>
        <CssBaseline />
        <Container fixed >
          <Typography component="div" className={classes.containerStyle}>
            {props.children}
          </Typography>
        </Container>

      </div>
      <Footer/>
    </Fragment>
  );
};

export default Layout;
