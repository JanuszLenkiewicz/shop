import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import Layout from './ancillaries/layout/layout'
import Auth from './ancillaries/auth/Auth';
import { Login } from './features/login';
import { Admin } from './features/admin';
import { Users } from './features/users';
import { MyCart } from './features/my-cart';
import { AddUser } from './features/add-user';
import { Home } from './core/home';
import { CreateCourse } from "./features/create-course";

function App() {
  return (
    <BrowserRouter>
      <Layout>
          <Switch>
            <Route path="/login"  component={Auth(Login, false)} />
            <Route path="/users"  component={Auth(Users, true, true, '/users')} />
            <Route path="/adduser"  component={Auth(AddUser, true, true, '/adduser')} />
            <Route path="/addcourse"  component={Auth(CreateCourse, true, true, '/addcourse')} />
            <Route path="/user/cart"  component={Auth(MyCart, false)} />
            <Route path="/admin"  component={Auth(Admin, true, true, '/admin')} />
            <Route path="/" exact component={Auth(Home, false)} />
            <Redirect to="/" />
          </Switch>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
