import React, {Component} from 'react';

import AddProduct from './components/AddProduct/AddProduct'

class Admin extends Component {
  render() {
    return (
      <div>
        <h1>Ustawienia systemu</h1>
        <AddProduct />
      </div>
    );
  }
}

export default Admin;
