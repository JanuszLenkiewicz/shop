import React, {Component} from 'react';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import classes from './AddProduct.css';

class Admin extends Component {

  state = {
    product: {
      name: '',
      count: ''
    }
  };

  handleChange = event => {
    console.log('EVENT',event.target);
    const product = {...this.state.product, [event.target.id]: event.target.value};
    this.setState({product});
  };


  render() {
    console.log('State', this.state);

    return (
      <form>
        <h2>Ustawienia kursów</h2>

        <TextField
          id="name"
          label="Kurs"
          className={classes.textField}
          value={this.state.product.name}
          onChange={this.handleChange}
          margin="normal"
        />

        <TextField
          id="count"
          label="Parametry"
          className={classes.textField}
          value={this.state.product.count}
          onChange={this.handleChange}
          margin="normal"
        />

        <Button variant="contained" className={classes.button}>
          Ustaw
        </Button>
      </form>
    );
  }
}

export default Admin;
