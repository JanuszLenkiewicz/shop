import { ACTION_CREATE_COURSE } from '../actions/create-course.actions';

export default function courseReducer(state=[], action) {
  switch (action.type) {
    case ACTION_CREATE_COURSE:
      return [...state, {...action.course}];

    default:
      return state;
  }
}
