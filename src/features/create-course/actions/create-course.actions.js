export const ACTION_CREATE_COURSE = 'ACTION_CREATE_COURSE';

export function createCourse(course) {
  return {type: ACTION_CREATE_COURSE, course}
}
