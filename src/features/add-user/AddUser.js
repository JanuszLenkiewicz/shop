import React, {Component, Fragment} from 'react';
import { connect } from "react-redux";
import { string } from 'prop-types';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';

import classes from './AddUser.css';
import { addUserService } from '../../utils/services/user.services'

class AddUser extends Component {
  state = {
    title: 'Dodaj użytkownika',
    snackbar: {
      vertical: 'top',
      horizontal: 'right',
      open: false,
      message: 'I love snacks'
    },
    user: {
      username: '',
      password: '',
      firstname: '',
      lastname: '',
      email: '',
      organization: 'VWP',
      courses: [
        {
          "status": 0,
          "scoreOfCourse": 0,
          "lastSlide": 1,
          "_id": "5d18bdc6154a7b32b208324d",
          "name": "5c6f3d302e7e9c8bed11d434"
        },
        {
          "status": 0,
          "scoreOfCourse": 0,
          "lastSlide": 1,
          "_id": "5d18bdc6154a7b32b208324c",
          "name": "5c6f3d472e7e9c8bed11d436"
        },
        {
          "status": 0,
          "scoreOfCourse": 0,
          "lastSlide": 1,
          "_id": "5d18bdc6154a7b32b208324b",
          "name": "5c6f3d632e7e9c8bed11d438"
        }
      ]
    }
  };

  handleChange = event => {
    console.log('EVENT',event.target);
    const user = {...this.state.user, [event.target.id]: event.target.value};
    this.setState({user});
  };

  sendData = e => {
    e.preventDefault();
    addUserService(this.state.user)
      .then(data => {
        console.log('Zapisano użytkownika', data);
        this.handleOpenSnackbar('Zapisano poprawnie użytkownika :)');
        const user = { ...this.state.user, username: '', password: '', firstname: '', lastname: '', email: ''};
        this.setState({user});
      })
      .catch(error => {
        this.handleOpenSnackbar('Błąd zapisu!!! Sprawdź czy nie zdublowano nazwy użytkownika.');
        console.log('LOGIN ERROR!', error);
        throw error;
      });
  };

  handleCloseSnackbar = () => {
    this.setState({ ...this.state.snackbar, open: false });
  };

  handleOpenSnackbar = (value) => {
    const snackbar = { ...this.state.snackbar, open: true, message: value };
    this.setState({snackbar});
  };

  render() {
    return (
      <Fragment>
        {
          this.state.title ?
            <div>
              <Typography variant="h4" gutterBottom className={classes.title}>
                {this.state.title}
              </Typography>
            </div>
            :null
        }

        <form className={classes.addUser}>

          <TextField
            id="username"
            label="Nazwa użytkownika"
            className={classes.textField}
            value={this.state.user.username}
            onChange={this.handleChange}
            margin="normal"
          />

          <TextField
            id="password"
            label="Hasło"
            className={classes.textField}
            value={this.state.user.password}
            onChange={this.handleChange}
            margin="normal"
          />

          <TextField
            id="firstname"
            label="Imię"
            className={classes.textField}
            value={this.state.user.firstname}
            onChange={this.handleChange}
            margin="normal"
          />

          <TextField
            id="lastname"
            label="Nazwisko"
            className={classes.textField}
            value={this.state.user.lastname}
            onChange={this.handleChange}
            margin="normal"
          />

          <TextField
            id="email"
            label="E-mail"
            className={classes.textField}
            value={this.state.user.email}
            onChange={this.handleChange}
            margin="normal"
          />

          <TextField
            id="organization"
            label="Organizacja"
            className={classes.textField}
            value={this.state.user.organization}
            onChange={this.handleChange}
            margin="normal"
          />

          <div className={classes.buttonSave}>
            <Button
              variant="contained"
              onClick={this.sendData}>
              Zapisz
            </Button>
          </div>
        </form>

        <Snackbar
          anchorOrigin={{vertical: this.state.snackbar.vertical, horizontal: this.state.snackbar.horizontal}}
          key={`${this.state.snackbar.vertical},${this.state.snackbar.horizontal}`}
          open={this.state.snackbar.open}
          onClose={this.handleCloseSnackbar}
          autoHideDuration={6000}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{this.state.snackbar.message}</span>}
        />

      </Fragment>
    );
  }
}

const mapDispatchToProps = {
  // newUser
};

AddUser.propTypes = {
  title: string,
  username: string,
  password: string,
  firstname: string,
  lastname: string,
  email: string,
  organization: string
};

export default connect(null, mapDispatchToProps) (AddUser);
