import { loginUserService } from '../../../utils/services/user.services'

export const ACTION_LOGIN_USER_SUCCESS = 'ACTION_LOGIN_USER_SUCCESS';
export const ACTION_LOGIN_USER_ERROR = 'ACTION_LOGIN_USER_ERROR';
export const ACTION_LOGOUT_USER = 'ACTION_LOGOUT_USER';

export const loginUserSuccess = data => {
  return {
    type: ACTION_LOGIN_USER_SUCCESS,
    payload: data
  }
};

export const loginUserError = error => {
  return {
    type: ACTION_LOGIN_USER_ERROR,
    payload: error
  }
};

export const logoutUser = () => {
  return {
    type: ACTION_LOGOUT_USER,
    payload: {}
  }
};

export const loginUser = (dataToSubmit) => {
  return dispatch => {
    return loginUserService(dataToSubmit)
      .then(data => {
        dispatch(loginUserSuccess(data));
      })
      .catch(error => {
        console.log('LOGIN ERROR!', error);
        dispatch(loginUserError(error));
        throw error;
      });
  }
};
