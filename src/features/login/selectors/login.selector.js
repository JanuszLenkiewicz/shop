import { get } from 'lodash';

export const getLogin = state => {
  return get(state, 'user');
};
