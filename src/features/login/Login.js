import React, {Component} from 'react';
import {shape, string, bool, oneOfType, number} from 'prop-types';
import { connect } from "react-redux";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';


import FormField from '../../ancillaries/form-field/formField';
import { update, generateData, isFormValid } from '../../utils/form/formActions';
import { loginUser } from './actions/login.actions';
import { getLogin } from './selectors/login.selector';
import classes from './Login.css';

class Login extends Component {

  state = {
    formError: false,
    formSuccess:'',
    formData: {
      username: {
        element: 'input',
        value: '',
        config:{
          id: 'user-name',
          name: 'User name',
          type: 'email',
          aria: 'user-name-text',
          placeholder: 'Enter your email',
          margin: 'normal'
        },
        validation:{
          required: true,
          email: true
        },
        valid: false,
        touched: false,
        validationMessage:''
      },
      password: {
        element: 'input',
        value: '',
        config:{
          id: 'user-password',
          name: 'Password',
          type: 'password',
          aria: 'user-password-text',
          placeholder: 'Enter your password',
          margin: 'normal'
        },
        validation:{
          required: true,
          minLongPassword: true
        },
        valid: false,
        touched: false,
        validationMessage:''
      }
    }
  };

  componentDidUpdate(prevProps) {
    if (this.props.login !== prevProps.login) {
      if (this.props.login.isAuth) {
        localStorage.setItem('isAuth', true);
        localStorage.setItem('role', this.props.login.role);
        localStorage.setItem('token', this.props.login.token);
        localStorage.setItem('userId', this.props.login.userId);
        this.props.history.push('/users')
      } else {
        this.setState({
          formError: true
        })
      }
    }
  }

  updateForm(element) {
    const newFormData = update(element,this.state.formData,'login');

    this.setState({
      formError: false,
      formData: newFormData
    })
  }

  submitForm= (event) =>{
    event.preventDefault();

    let dataToSubmit = generateData(this.state.formData,'login');
    let formIsValid = isFormValid(this.state.formData,'login')

    if(formIsValid){
      this.props.loginUser(dataToSubmit);
    } else {
      this.setState({
        formError: true
      })
    }
  };

  render() {
    return (
      <div className={classes.loginContainer}>
        <Card className={classes.card}>
          <CardContent>
            <div className={classes.matCardHeader}>
              <Typography  variant="h5" component="h1">
                Login
              </Typography>
            </div>
            <div className={classes.matCardContent}>
              <div>
                <FormField
                  id={'username'}
                  formData={this.state.formData.username}
                  change={this.updateForm.bind(this)}
                />

                <FormField
                  id={'password'}
                  formData={this.state.formData.password}
                  change={this.updateForm.bind(this)}
                />

                { this.state.formError ?
                  <div>
                    Please check your data
                  </div>
                  : null}

                <div className={classes.buttonArea}>
                  <Button
                    variant="contained"
                    className={classes.button}
                    onClick={this.submitForm.bind(this)}
                  >
                    Log in
                  </Button>
                </div>
              </div>
            </div>
          </CardContent>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    login: getLogin(state)
  }
};

const mapDispatchToProps = {
  loginUser
};

Login.propTypes = {
  login: shape({
    role: oneOfType([string,number]),
    token: string,
    userId: string,
    isAuth: bool
  })
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
