import {
  ACTION_LOGIN_USER_SUCCESS,
  ACTION_LOGIN_USER_ERROR,
  ACTION_LOGOUT_USER
} from "../actions/login.actions";

export default function reducerLoginUser(state = {}, action) {
  switch (action.type) {
    case ACTION_LOGIN_USER_SUCCESS:
      return action.payload.data;
    case ACTION_LOGIN_USER_ERROR:
      return {
        isError: true,
        error: action.payload
      };
    case ACTION_LOGOUT_USER:
      return action.payload;
    default:
      return state;
  }
}
