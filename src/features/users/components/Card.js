import React from 'react';
import { string, number, array, oneOfType } from 'prop-types';
import { connect } from 'react-redux';

import {getUser} from "../selectors/user.selector";
// import classes from './Card.css';

const Card = (props) => {
  console.log(props);

  return (
    <div>
      {props.firstname} {props.lastname}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    user: getUser(state)
  }
};

Card.propTypes = {
  name: string,
  description: string,
  images: oneOfType([array, string]),
  brand: string,
  price: number,
};

export default connect(mapStateToProps)(Card);
