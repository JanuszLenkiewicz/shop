import { usersService } from '../../../utils/services/users.services'

export const ACTION_GET_USERS_SUCCESS = 'ACTION_GET_USERS_SUCCESS';
export const ACTION_GET_USERS_ERROR = 'ACTION_GET_USERS_ERROR';

export const usersSuccess = data => {
  return {
    type: ACTION_GET_USERS_SUCCESS,
    payload: data
  }
};

export const usersError = error => {
  return {
    type: ACTION_GET_USERS_ERROR,
    payload: error
  }
};

export const users = () => {
  return dispatch => {
    return usersService()
      .then(data => {
        dispatch(usersSuccess(data));
      })
      .catch(error => {
        console.log('LOGIN ERROR!', error);
        dispatch(usersError(error));
        throw error;
      });
  }
};
