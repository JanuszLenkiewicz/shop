import { get } from 'lodash';

export const getUser = state => {
  return get(state, 'user');
};
