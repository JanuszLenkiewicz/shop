import { get } from 'lodash';

export const getUsers = state => {
  return get(state, 'users');
};
