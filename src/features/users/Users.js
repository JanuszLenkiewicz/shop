import React, {Component, Fragment} from 'react';
import { connect } from "react-redux";
import { func } from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import { users } from './actions/users.actions';
import { getUsers } from './selectors/users.selector';
import classes from './Users.css';

class Users extends Component {
  state = {
    users:[],
    title: 'Użytkownicy'
  };

  componentDidMount() {
    this.props.users();
  }

  componentDidUpdate(prevProps) {
    if(prevProps !== this.props) {
      const users = {...this.state.users, users: this.props.usersFetched.value};
      this.setState(users);
    }
  }

  renderCards(users) {
    return users ?
      users.map(row => (
        <TableRow key={row._id}>
          <TableCell align="left">{row.username}</TableCell>
          <TableCell align="left">{row.firstname}</TableCell>
          <TableCell align="left">{row.lastname}</TableCell>
          <TableCell align="left">{row.email}</TableCell>
          <TableCell align="left">{row.birthData}</TableCell>
          <TableCell align="left">{row.birthPlace}</TableCell>
        </TableRow>
      ))
      : null
  };

  render() {
    return (
      <Fragment>
          {
            this.state.title ?
              <div>
                <Typography variant="h4" gutterBottom className={classes.title}>
                  {this.state.title}
                </Typography>
              </div>
              :null
          }
          <div>


            <Paper className={classes.root}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell align="left">Nazwa</TableCell>
                    <TableCell align="left">Imię</TableCell>
                    <TableCell align="left">Nazwisko</TableCell>
                    <TableCell align="left">E-mail</TableCell>
                    <TableCell align="left">Data ur</TableCell>
                    <TableCell align="left">Miejsce ur</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  { this.renderCards(this.state.users) }
                </TableBody>
              </Table>
            </Paper>

          </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    usersFetched: getUsers(state)
  }
};

const mapDispatchToProps = {
  users
};

Users.propTypes = {
  users: func
};

export default connect(mapStateToProps, mapDispatchToProps) (Users);
