import {
  ACTION_GET_USERS_SUCCESS,
  ACTION_GET_USERS_ERROR
} from "../actions/users.actions";

export default function reducerUsers(state = [], action) {
  switch (action.type) {
    case ACTION_GET_USERS_SUCCESS:
      return action.payload.data;
    case ACTION_GET_USERS_ERROR:
      return {
        isError: true,
        error: action.payload
      };
    default:
      return state;
  }
}
