import React, {Component} from 'react';
import { Link, withRouter} from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { shape, func } from 'prop-types';

import classes from './Header.css';
import { connect } from 'react-redux';
import { logoutUser } from '../../features/login/actions/login.actions';
import { getLogin } from '../../features/login/selectors/login.selector';

class Header extends Component {
  state = {
    page:[],
    user:[
      {
        name:'Profil',
        linkTo:'/user/cart',
        public: false
      },
      {
        name:'Zaloguj',
        linkTo:'/login',
        public: true
      },
      {
        name:'Wyloguj',
        linkTo:'/',
        public: false
      },
    ],
    admin: [
      {
        name:'Użytkownicy',
        linkTo:'/users',
        public: false
      },
      {
        name:'Dodaj użytkownika',
        linkTo:'/adduser',
        public: false
      },{
        name:'Dodaj kurs',
        linkTo:'/addcourse',
        public: false
      }
    ],
    auth: {
      isAuth: false,
      role: '0'
    }
  };

  componentDidMount() {
      const isAuth = localStorage.getItem('isAuth');
      const role = localStorage.getItem('role');
      if(isAuth) {
        console.log('Is auth');
        this.setState({
          auth: {
            isAuth: isAuth,
            role: role
          }
        });
      }
  }

  componentDidUpdate(prevProps) {
    if(prevProps.user !== this.props.user){
      this.setState({
        auth: {
          isAuth: this.props.user.isAuth,
          role: this.props.user.role
        }
      });
      this.forceUpdate();
    }
  }

  logoutHandler = () => {
    localStorage.setItem('isAuth', '');
    localStorage.setItem('role', '');
    localStorage.setItem('userId', '');
    localStorage.setItem('token', '');
    this.setState({
      auth: {
        isAuth: '',
        role: null
      }
    });
    this.props.logoutUser();
    this.props.history.push('/');
  };


  cartLink = (item,i) => {
    return (
      <div key={i}>
        <Link to={item.linkTo}><Button color="inherit">
          {item.name}</Button>
        </Link>
      </div>
    )
  };


  defaultLink = (item,i) => (
    item.name === 'Wyloguj' ?
      <Button color="inherit"
           key={i}
           onClick={()=> this.logoutHandler()}
      >
        {item.name}
      </Button>

      :
      <Link to={item.linkTo} key={i}>
        <Button color="inherit">{item.name}</Button>
      </Link>
  );


  showLinks = (type) =>{
    let list = [];

      type.forEach((item)=>{
        if(!this.state.auth.isAuth){
          if(item.public === true){
            list.push(item)
          }
        } else{
          if(item.name !== 'Zaloguj'){
            list.push(item)
          }
        }
      });

    return list.map((item,i)=>{
      if(item.name !== 'My Cart'){
        return this.defaultLink(item,i)
      } else {
        return this.cartLink(item,i)
      }

    })
  };

  showLinksAdmin = (type) => {
    if(this.state.auth.role === '1' || this.state.auth.role === 1) {
      return type.map((item, id) => {
        return (
          <Link to={item.linkTo} key={id}>
            <Button color="inherit">{item.name}</Button>
          </Link>
        )
      })
    }
  };

  render() {
    return (
      <header>
        <div className={classes.root}>
          <CssBaseline />
          <AppBar position="static">
            <Toolbar>
              <Typography variant="h6" className={classes.title}>
                Szkoleniowiec
              </Typography>
              <div className={classes.inside}>
                <div className={classes.left}>
                  {this.state.page && this.showLinks(this.state.page)}
                  {this.state.admin && this.showLinksAdmin(this.state.admin)}
                </div>
                <div className={classes.right}>
                  {this.showLinks(this.state.user)}
                </div>

              </div>
            </Toolbar>
          </AppBar>
        </div>
      </header>
    );
  }
}

function mapStateToProps(state){
  return {
    user: getLogin(state)
  }
}

const mapDispatchToProps = {
  logoutUser
};

Header.propTypes = {
  user: shape({}),
  logoutUser: func
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header));
