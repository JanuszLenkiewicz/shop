import axios from 'axios';

import { URL_API } from "../global.config";

export function loginUserService(dataToSubmit) {
  return axios.post(URL_API + '/user/userlog', dataToSubmit)
};

export function addUserService(dataToSubmit) {
  return axios.post(URL_API + '/user/signup', dataToSubmit)
};
