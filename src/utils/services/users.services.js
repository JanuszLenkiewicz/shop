import axios from 'axios';

import { URL_API } from "../global.config";

export function usersService(data) {
  return axios.get(URL_API + '/user', data)
};
